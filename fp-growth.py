#-*-coding:utf-8-*-
import numpy as npy
import pandas as  pda
import copy
import time

class TreeNode:
    def __init__(self,name,count,parent):
        self.name = name
        self.count = 1
        self.parent = parent
        self.nodelink = None
        self.child = {}

    def inc(self):
        self.count += 1

    def disp(self,ntab = 1):
        print(" "*ntab,self.name,self.count)
        for key in self.child:
            self.child[key].disp(ntab+1)

def filterdata():
    data = pda.read_excel("data/超市数据集.xls",encoding="UTF-8",header=None)
    data = npy.array(data.T[1:].T)
    goodsidx = data[0,:]
    goodsname = data[1,:]
    goodsdata = data[2:,:]
    a,b = npy.shape(goodsdata)
    datalist = []
    dataset = []
    for i in range(0,a):
        datalist.append(set(goodsidx[goodsdata[i] == "T"]))
    for dl in datalist:
        #print(dl)
        dataset.extend([frozenset([w]) for w in dl])
        #print([frozenset([w]) for w in dl])
    dataset = set(dataset)
    #for da in dataset:
        #print(da)
    return datalist,dataset

def createTree(datalist,level):
    headertab = {}
    for items in datalist:
        for item in items:
            headertab[item] = headertab.get(item,0)+datalist[items]
    dataset = set(headertab.keys())
    #print(dataset)
    for k in dataset:
        #print([headertab[k],None])
        if headertab[k]<level:
            del(headertab[k])
        else:
            headertab[k] = [headertab[k],None]
            #print(headertab[k])
    retTree = TreeNode("root",1,None)
    for items in datalist:
        itemD = {}
        for item in items:
            if item in headertab:
                itemD[item] = headertab[item][0]
        if len(itemD) > 0:
            sortedItems = [item[0] for item in sorted(itemD.items(),key=lambda p:p[1],reverse=True)]
            updateTree(sortedItems,retTree,headertab)
    return retTree,headertab

def updateTree(itemlist,rTree,headertable):
    #print(headertable[itemlist[0]])
    if itemlist[0] in rTree.child:
        rTree.child[itemlist[0]].inc()
    else:
        rTree.child[itemlist[0]] = TreeNode(itemlist[0],1,rTree)
        if headertable[itemlist[0]][1]==None:
            headertable[itemlist[0]][1] = rTree.child[itemlist[0]]
            #print(headertable[itemlist[0]][1].name)
        else:
            #print(headertable[itemlist[0]][1].name)
            updateHeader(headertable[itemlist[0]][1],rTree.child[itemlist[0]])
    if len(itemlist) > 1:
        updateTree(itemlist[1:],rTree.child[itemlist[0]],headertable)

def updateHeader(treeNode,newNode):
    #print("start update")
    count = 1
    while treeNode.nodelink!=None:
        treeNode  = treeNode.nodelink
        count +=1
       # print(treeNode==treeNode.nodelink)
    treeNode.nodelink = newNode
    #print("endupdate")
def prefixPath(node,path):
    if node.parent!=None:
        path.append(node.name)
        prefixPath(node.parent,path)
def findPrefixPath(leafNodes,name):
    retpat = {}
    leaf = leafNodes
    while leaf.nodelink != None:
        count = leaf.count
        leaf = leaf.nodelink
        prefixpath = []
        prefixPath(leaf.parent,prefixpath)
        if len(prefixpath) > 0:
            pset = frozenset(prefixpath)
            retpat[pset] = count
    return retpat

def relat(minval,tree,headerTab,memlist=[]):
    sortedHeaderTab = sorted(headerTab.items(),key=lambda p:p[1][0])
    #print(sortedHeaderTab)

    for headertabline in sortedHeaderTab:
        cmem = copy.deepcopy(memlist)
        cmem.append(headertabline[0])
        prefixPlist = findPrefixPath(headertabline[1][1],headertabline[0])
        mTree,mHeaderTab = createTree(prefixPlist,minval)
        if len(mHeaderTab.items()) > 0:
            relat(minval,tree,mHeaderTab,cmem)
        print("condition tree for "+cmem.__str__()+":")
        mTree.disp()
        #print("\n")

def main():
    datalist,dataset = filterdata()
    #datalist = [["r","z","h","j","p"],["z","y","x","w","v","u","t","s"],["z"],["r","x","n","o","s"],["y","r","x","z","q","t","p"],["y","z","x","e","q","s","t","m"]]
    datadic = {frozenset(data):1 for data in datalist}
   # print(datadic)
    startT = time.time()
    mTree,mHeaderTab = createTree(datadic,90)
    #mTree.disp()
    relat(90,mTree,mHeaderTab)
    endT = time.time()
    print("fp-growth的时间消耗为：",endT-startT)

main()





