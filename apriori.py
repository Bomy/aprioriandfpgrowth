# -*- coding:utf-8 -*-
import pandas as pda
import numpy as npy
import copy
import time

def initdata():
    data = pda.read_excel("data/超市数据集.xls",encoding="UTF-8",header=None)
    data = npy.array(data.T[1:].T)
    goodsidx = data[0,:]
    goodsname = data[1,:]
    goodsdata = data[2:,:]
    a,b = npy.shape(goodsdata)
    print(a,b)
    rst = npy.zeros((a,b))
    rst[goodsdata == "T"] = 1
    goodsdata = rst
    print(type(goodsname),goodsidx[goodsdata[0]==1.])
def filterdata():
    data = pda.read_excel("data/超市数据集.xls",encoding="UTF-8",header=None)
    data = npy.array(data.T[1:].T)
    goodsidx = data[0,:]
    goodsname = data[1,:]
    goodsdata = data[2:,:]
    goodsdic = dict(zip(goodsidx,goodsname))
    a,b = npy.shape(goodsdata)
    datalist = []
    dataset = []
    for i in range(0,a):
        datalist.append(set(goodsidx[goodsdata[i] == "T"]))
    for dl in datalist:
        #print(dl)
        dataset.extend([frozenset([w]) for w in dl])
        #print([frozenset([w]) for w in dl])
    dataset = set(dataset)
    #for da in dataset:
        #print(da)
    return datalist,dataset,goodsdic
def surportdata(dataset,datalist,retdic = {},k=1,key=1,dcount = 1,l = 0.5):
    retdic[key] = {}
    newlist = list(dataset)
    newset = set()
    if key!=1:
        print("k:",k)
        for i in range(0,len(dataset)):
            for j in range(i+1,len(dataset)):
                
                if k <= 2:
                    print(list(newlist[i])[:k-2],list(newlist[j])[:k-2])
                    newset.add(frozenset((newlist[i]|newlist[j])))
                elif list(newlist[i])[:k-2] == list(newlist[j])[:k-2] and k-1==len(newlist[j]):
                    print(list(newlist[i])[:k-2],list(newlist[j])[:k-2])
                    newset.add((newlist[i]|newlist[j]))
        dataset = newset
    for w in dataset:
        for line in datalist:
            line = frozenset(line)
            if w.issubset(line):
                #print(w)
                retdic[key][w] = retdic[key].get(w,0)+1
    print(retdic)
    for ky in dataset:
        if ky in retdic[key]:
            if retdic[key][ky]/dcount < l:
                print(retdic[key][ky]/dcount)
                del(retdic[key][ky])
            else:
                retdic[key][ky] = retdic[key][ky]/dcount
    dataset = set([ky for ky in retdic[key]])
    #print(dataset)
    if len(dataset)>1:
        surportdata(dataset,datalist,retdic,k+1,key+1,dcount,l=l)
    else:
        return retdic

def belidata(surportdatadic,minlevel):
    belires = []
    copydata = copy.deepcopy(surportdatadic)
    del(copydata[1])
    for key in copydata:
        for ri in set([item[0] for item in copydata[key].items()]):
            for i in range(1,key):
                for li in  set([item[0] for item in surportdatadic[i].items()]):
                    if li.issubset(ri):
                        belival  = copydata[key][ri]/surportdatadic[i][li]
                        belires.append([li,ri-li,belival])
    return belires
            
            
    
def apriori(datalist):
    dataset = []
    for line in datalist:
        #print(list(line))
        dataset.extend([frozenset([w]) for w in list(line)])
    dataset = set(dataset)
    retdic = {}
    surportdata(dataset=dataset,datalist=datalist,retdic = retdic,dcount = len(datalist),l = 0.4)
    belires = belidata(retdic,1.0)
    return retdic,belires
def test():
    #datalist = [["r","z","h","j","p"],["z","y","x","w","v","u","t","s"],["r","x","n","o","s"],["y","r","x","z","q","t","p"],["y","z","x","e","q","s","t","m"]]
    datalist,dataset,goodsdic = filterdata()
    startT = time.time()
    retdic,belires = apriori(datalist)
    endT = time.time()
    for key in retdic:
        for k in retdic[key]:
            idx2name = []
            for idx in k:
                idx2name.append(goodsdic[idx])
            print("**********************************************")
            print("频繁"+str(key)+"项集支持度：",idx2name,retdic[key][k])
            print("**********************************************\n")
    for line in belires:
        idx2name0 = []
        idx2name1 = []
        for idx in line[0]:
            idx2name0.append(goodsdic[idx])
        for idx in line[1]:
            idx2name1.append(goodsdic[idx])
        print("**********************************************")
        print("置信度",idx2name0,"-->",idx2name1,":",line[2])
        print("**********************************************\n")
    print("apriori耗时为：",endT-startT)
test()
